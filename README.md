#create cassanda tables 
```sh
/usr/local/cassandra/cql

#create keyspace
create keyspace ycsb
        WITH REPLICATION = {'class' : 'SimpleStrategy', 'replication_factor': 3 };

#create table
USE ycsb;
create table usertable (
        y_id varchar primary key,
        field0 varchar,
        field1 varchar,
        field2 varchar,
        field3 varchar,
        field4 varchar,
        field5 varchar,
        field6 varchar,
        field7 varchar,
        field8 varchar,
        field9 varchar);




# cassandra_ycsb load database

 ./bin/ycsb load cassandra-cql -threads 4 -P workloads/workloada -p hosts="localhost"
 
```

